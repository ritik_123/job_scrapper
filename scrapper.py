from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from multiprocessing import Process
import csv



def job_scrapper(job_url, job_location):
	try:
		job_titles=['Executive+Assistant', 'Program+Manager', 'Network+Engineer', 'Office+Assistant', 'Systems+Administrator', 'Operations+Manager', 'District+Manager', 'Software+Developer', 'Software+Engineer', 'Administrative+Assistant', 'Assistant+General+Manager', 'Office+Administrator', 'Project+Manager', 'General+Manager', 'Business+Analyst', 'Front+Desk+Receptionist', 'Data+Analyst', 'Warehouse+Associate', 'Dispatcher', 'Assembler', 'Warehouse+Supervisor', 'Warehouse+Clerk', 'Quality+Assurance+Specialist', 'Shipping+and+Receiving+Clerk', 'Machine+Operator', 'Forklift+Operator', 'Executive+Director', 'Manufacturing+Engineer', 'CNC+Machinist', 'Machinist', 'Warehouse+Worker', 'Driver', 'Office+Manager', 'Maintenance+Mechanic', 'Merchandiser', 'Material+Handler', 'Diesel+Mechanic', 'Logistics+Coordinator', 'Buyer', 'Housekeeper', 'General+Laborer', 'Leasing+Agent', 'Graphic+Designer', 'Front+Desk+Agent', 'Pharmacy+Technician', 'Pharmacist', 'Programmer+Analyst', 'Kennel+Attendant', 'Research+Associate', 'Web+Developer', 'Business+Systems+Analyst', 'Kennel+Assistant', 'Guest+Service+Agent', 'Human+Resources+Coordinator', 'System+Administrator', 'Customer+Service+Representative', 'Product+Manager', 'UX+Designer', 'Database+Administrator', 'Data+Scientist', 'ETL+Developer', 'Systems+Engineer', 'Technical+Writer', 'Instructional+Designer', 'Java+Developer', 'Process+Engineer', 'Mechanical+Engineer', 'Security+Officer', 'Sales+Associate', 'Welder', 'Store+Manager', 'Bookkeeper', 'Hair+Stylist', 'Esthetician', 'Quality+Inspector', 'Cashier', 'Floral+Designer', 'Mortgage+Loan+Officer', 'Administrative+Coordinator', 'Leasing+Consultant', 'Marketing+Manager', 'Teller', 'Account+Manager', 'Restaurant+Manager', 'Receptionist', 'Bartender', 'Loan+Processor', 'Field+Service+Technician', 'Quality+Control+Inspector', 'Delivery+Driver', 'Automotive+Technician', 'Production+Supervisor', 'Electronic+Assembler', 'Nurse+Practitioner', 'Dental+Hygienist', 'Night+Auditor', 'Retail+Sales+Associate', 'Office+Manager/Bookkeeper', 'Staff+Accountant', 'Registered+Dental+Hygienist', 'Medical+Technologist', 'Physical+Therapist', 'Assistant+Controller', 'Accountant', 'Senior+Accountant', 'Dental+Assistant', 'Occupational+Therapist', 'Medical+Assistant', 'Accounts+Payable+Clerk', 'Full+Charge+Bookkeeper', 'Accounting+Assistant', 'Bookkeeper/Administrative+Assistant', 'Program+Coordinator', 'Paralegal', 'Senior+Project+Manager', 'Help+Desk+Technician', 'IT+Support+Specialist', 'Project+Engineer', 'Maintenance+Technician', 'Assistant+Store+Manager', 'Electrical+Engineer', 'Sales+Consultant', 'Marketing+Assistant', 'Sales+Representative', 'Licensed+Esthetician', 'Sales+Coordinator', 'Assistant+Property+Manager', 'Sales+Manager', 'Marketing+Coordinator', 'Line+Cook', 'Cook', 'Executive+Chef', 'Carpenter', 'Groundskeeper', 'Line+Cook/Prep+Cook', 'Dishwasher', 'Human+Resources+Assistant', 'Senior+Software+Engineer', 'Custodian', 'Painter', 'Construction+Project+Manager', 'Property+Manager', 'Construction+Superintendent', 'HVAC+Technician', 'Baker', 'Assistant+Manager', 'Kitchen+Manager', 'Sous+Chef', 'Human+Resources+Generalist', 'Industrial+Electrician', 'Electrician', 'Food+Service+Worker', 'Inside+Sales+Representative', 'Registered+Nurse', 'Server+and+Bartender', 'Service+Technician', 'Recruiter', 'Steward', 'Mortgage+Loan+Processor', 'Server', 'Manager', 'Mechanic', 'Janitor', 'Journeyman+Electrician', 'Interior+Designer', 'Heavy+Equipment+Operator', 'Estimator', 'Human+Resources+Manager', 'HR+Coordinator', 'Laborer', 'Nuclear+Medicine+Technologist', 'Financial+Analyst', 'Massage+Therapist', 'Finance+Manager', 'Billing+Specialist', 'Licensed+Massage+Therapist', 'Controller', 'Case+Manager', 'Senior+Financial+Analyst', 'Chief+Financial+Officer', 'Accounting+Manager', 'Accounts+Receivable+Clerk', 'Operations+Supervisor', 'Legal+Secretary', 'HR+Generalist', 'Project+Coordinator', 'Lifeguard', 'Shipping+Clerk', 'Warehouse+Assistant', 'Nanny', 'Kennel+Technician', 'Account+Executive', 'Research+Assistant', 'Litigation+Paralegal', 'Veterinary+Receptionist', 'Medical+Receptionist', 'Veterinary+Assistant', 'Business+Development+Manager', 'Veterinary+Technician', 'Social+Worker', 'CAD+Drafter', 'Phlebotomist', 'Quality+Engineer', 'Welder+and+Fabricator', 'Maintenance+Worker', 'Outside+Sales+Representative', 'Marketing+Specialist', 'Copywriter', 'Branch+Manager', 'Laboratory+Technician', 'Executive+Administrative+Assistant', 'Maintenance+Supervisor', 'Production+Manager', 'Respiratory+Therapist', 'Director+of+Operations', 'Office+Coordinator', 'Purchasing+Agent', 'Recruiting+Coordinator', 'Valet+Attendant', 'Security+Guard', 'Medical+Records+Clerk', 'Unarmed+Security+Officer', 'Valet+Parking+Attendant', 'Desktop+Support+Technician', 'Software+Development+Engineer', 'Treasury+Analyst', 'Legal+Assistant', 'Yoga+Instructor', 'Data+Entry+Clerk', 'Accounting+Clerk']
		d = DesiredCapabilities.CHROME
		d['loggingPrefs'] = {'browser': 'ALL'}
		chrome_options = Options()
		chrome_options.add_argument("--window-size=1920,1080")
		chrome_options.add_argument("--disable-notifications")
		chrome_options.add_argument("--start-maximized")
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--disable-dev-shm-usage')
		chrome_prefs = {}
		chrome_options.experimental_options["prefs"] = chrome_prefs
		chrome_prefs["profile.default_content_settings"] = {"images": 2}
		chrome_prefs["profile.managed_default_content_settings"] = {"images": 2}
		driver = webdriver.Chrome(desired_capabilities=d, chrome_options=chrome_options)
		filename = 'indeed_job/data215.csv'
		for j in range(220, 230):
			url_new = job_url + '?q={}&l={}'.format(job_titles[j], '+'.join(job_location.split()))
			driver.get(url_new)
			page = 1
			fields = ['Job_title', 'company', 'summary', 'salary', 'job_description', 'date', 'location']
			job_list = []
			while page <= 30:
				try:
					job_card = WebDriverWait(driver, 5). \
						until(EC.visibility_of_all_elements_located((By.CLASS_NAME, "jobsearch-SerpJobCard")))
				except:
					break
				for job in job_card:
					job_title = get_title(job)
					job_description = get_job_description(job, driver)
					company = get_company(job)
					summary = get_summary(job)
					salary = get_salary(job)
					location = get_location(job)
					date = get_date(job)
					data = {'Job_title': job_title, 'company': company, "summary": summary, 'salary': salary, 'job_description': job_description, 'date': date, 'location': location}
					# print(json.dumps(data, sort_keys=True, indent=4))
					job_list.append(data)

				try:
					next_page_class = WebDriverWait(driver, 5). \
						until(EC.visibility_of_all_elements_located((By.CLASS_NAME, "np")))
				except:
					break
				out = False

				for link in next_page_class:
					if link.text == 'Next »' and page < 5:
						link.click()
						driver.get(driver.current_url)
					else:
						if len(next_page_class) == 1:
							out = True
				if out:

					break
				page += 1

			if j % 5 == 0:
				filename = 'indeed_job/data{}.csv'.format(j)
			with open(filename, 'a+') as csvfile:
				writer = csv.DictWriter(csvfile, fieldnames=fields, delimiter='Ü', lineterminator='\n\n')
				writer.writeheader()
				writer.writerows(job_list)


	except Exception as e:
		print({
			'status': False,
			'msg': e
		})


def get_title(job):
	try:
		title = job.find_element_by_class_name('turnstileLink').text.encode("utf-8")
		return title
	except:
		return ''


def get_company(job):
	try:
		company = job.find_element_by_class_name('company').text.encode("utf-8")
		return company
	except:
		return ''


def get_summary(job):
	try:
		summary = job.find_element_by_class_name('summary').text.encode("utf-8")
		return summary
	except:
		return ''


def get_location(job):
	try:
		location = job.find_element_by_class_name('location').text.encode("utf-8")
		return location
	except:
		return ''


def get_salary(job):
	try:
		salary = job. find_element_by_class_name('salary').text.encode("utf-8")
		return salary
	except:
		return ''


def get_job_description(job, driver):
	try:
		try:
			driver.find_element_by_id('popover-link-x').click()
		except:
			pass
		job.click()
		job_description = WebDriverWait(driver, 30). \
			until(EC.visibility_of_element_located((By.ID, "vjs-desc"))).text.encode("utf-8")
		return job_description

	except:
		return ''


def get_date(job):
	try:
		date = job.find_element_by_class_name('date').text.encode("utf-8")
		return date
	except:
		return ''


if __name__ == '__main__':

	url = 'https://www.indeed.com/jobs'
	ind_url = 'https://www.indeed.co.in/jobs'

	ind_location = ['Banglore', 'Delhi', 'Mumbai', 'Chennai', 'Pune', 'Hyderabad', 'Jaipur']

	location = ['New York', 'San Francisco', 'Amsterdam', 'London', 'Boston', 'Vancouver', 'Washington%2C DC']
	# location = ['New York', 'San Francisco',  'Boston', 'Washington%2C DC']

	Pros = []
	for i in range(len(location)):
		print("Thread {} Started".format(i))
		p = Process(target=job_scrapper, args=(url, location[i]))
		Pros.append(p)
		p.start()

	# block until all the threads finish (i.e. block until all function_x calls finish)
	for t in Pros:
		t.join()
